package S1A1;

import java.util.Scanner;

public class AverageComputer {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);

        // input first name
        System.out.println("First name: ");
        String firstName = userInput.nextLine();

        // input last name
        System.out.println("Last name: ");
        String lastName = userInput.nextLine();

        // input first subject grade
        System.out.println("First Subject Grade: ");
        double firstSubjectGrade = userInput.nextDouble();

        // input second subject grade
        System.out.println("Second Subject Grade: ");
        double secondSubjectGrade = userInput.nextDouble();

        // input third subject grade
        System.out.println("Third Subject Grade: ");
        double thirdSubjectGrade = userInput.nextDouble();

        double gradeAverage = (firstSubjectGrade + secondSubjectGrade + thirdSubjectGrade)/3;

        // print
        System.out.println("Good day" + firstName + lastName + ". Your grade average is: " + gradeAverage);
    }
}
