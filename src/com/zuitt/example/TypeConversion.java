package com.zuitt.example;

import java.util.Scanner;

public class TypeConversion {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);

        // 1st method
        System.out.println("How old are you? ");
        double age = Double.parseDouble(userInput.nextLine());
        System.out.println("This is a confirmation that you are " + age + " years old.");

        // 2nd method
        System.out.println("How old are you? ");
        double ageDouble = userInput.nextDouble();
        System.out.println("This is a confirmation that you are " + ageDouble + " years old.");

        //
        System.out.println("Enter a first number ");
        int num1 = userInput.nextInt();

        System.out.println("Enter a second number " );
        int num2 = userInput.nextInt();

        int sum = num1 + num2;
        System.out.println("The sum of both numbers are: " +sum);
    }
}
